import React, { Component } from 'react';
import './App.css';
import Header from "./components/presentation/header";
import HumansDataContainer from "./components/container/humansDataContainer";
import StationDataContainer from "./components/container/stationDataContainer";

// header presentation componnet

// component to fetch data on humans in space (and save it on state?), 
// presentation component to show it

// component to fetch data on position of space station and referesh it (and save it into state?)
// component to show data on space station



class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <HumansDataContainer />
        <StationDataContainer />
      </div>
    );
  }
}

export default App;
