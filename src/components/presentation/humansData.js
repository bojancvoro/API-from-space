import React from "react";

const HumansData = ({ humansInSpace, isLoading, error }) => {
    return (
        <div>
            {isLoading ? "Data loading" :
                error ? error.message :
                <div>
                    <h3>Currently there are {humansInSpace.length} people in space:</h3>
                    <ul>
                        {humansInSpace.map((person) => {
                            return <li key={person.name}>{person.name}</li>
                        })}
                    </ul>
                </div>
            }
        </div>
    );
}

export default HumansData;