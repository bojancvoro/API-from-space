import React from "react";

const StationData = ({ longitude, latitude, isLoading, error }) => {
    return (
        <div>
            <h3>Current position of the International Space Station:</h3>
            {isLoading ? "Data loading" :
            error ? error.measage:
            <div>
                <p>Longitude: {longitude}</p>
                <p>Latitude: {latitude}</p>
            </div>
        }
        </div>
    );
}

export default StationData;