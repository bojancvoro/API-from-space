import React, { Component } from "react";
import HumansData from "../presentation/humansData";

class HumansDataContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            humansInSpace: [],
            isLoading: true,
            error: null
        }
    }

    fetchHumansData() {
        fetch("http://api.open-notify.org/astros.json")
            .then(response => response.json())
            .then(data => this.setState({
                humansInSpace: data.people,
                isLoading: false
            }))
            .catch(error => this.setState({
                error,
                isLoading: false
            }));
    }

    componentDidMount() {
        this.fetchHumansData();
    }

    render() {
        const { humansInSpace, isLoading, error } = this.state;
        return (
            <div>
                <HumansData
                    humansInSpace={humansInSpace}
                    isLoading={isLoading}
                    error={error}
                />
            </div>
        );
    }
}

export default HumansDataContainer;