import React, { Component } from "react";
import StationData from "../presentation/stationData";

class StationDataContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude: "",
            longitude: "",
            isLoading: true,
            error: null
        }
    }

    fetchISSData = () => {
        fetch("http://api.open-notify.org/iss-now.json")
            .then(response => response.json())
            .then(data => this.setState(
                {
                    latitude: data.iss_position.latitude,
                    longitude: data.iss_position.longitude,
                    isLoading: false
                }
            ))
            .catch(error => this.setState(
                {
                    error,
                    isLoading: false
                }
            ));
    }

    componentDidMount() {
        this.fetchISSData();
        setInterval(this.fetchISSData, 3000);
    }

    render() {
        const {latitude, longitude, isLoading, error } = this.state;
        return (
            <div>
                <StationData
                    latitude={latitude}
                    longitude={longitude}
                    isLoading={isLoading}
                    error={error}
                />
            </div>
        );
    }
}

export default StationDataContainer;